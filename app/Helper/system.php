<?php

use App\Admin;
use App\Setting;
use Illuminate\Support\Facades\Input;

function getSetting($key){
    return Setting::getSetting($key)->value;
}


function getDataTable($items , $relations= []){
    $pagination = Input::get('pagination');
    $query = Input::get('query');

    $search = $query['generalSearch'];


    $sort = Input::get('sort');


    if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
        $pagination['perpage'] = 10;
    }
    $items = $items;


    if ($search != null) {
        $items->filter($search);
    }

    if ($sort && count($sort)) {
        $items->orderBy($sort['field'], $sort['sort']);
    } else {
        $items->orderByDesc('created_at');
    }

    $itemsCount = $items->count();
    $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
    $pagination['total'] = $itemsCount;
    $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

    $data['meta'] = $pagination;
    $data['data'] = $items;
    return $data;
}

function human_filesize($size, $precision = 2) {
    static $units = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $step = 1024;
    $i = 0;
    while (($size / $step) > 0.9) {
        $size = $size / $step;
        $i++;
    }
    return round($size, $precision).$units[$i];
}
