<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name' , 'code' , 'teacher_id' , 'is_accepted'];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class , 'group_students');
    }

    public function files()
    {
        return $this->hasMany(GroupFiles::class);
    }

    public function posts()
    {
        return $this->hasMany(GroupPosts::class);
    }
}
