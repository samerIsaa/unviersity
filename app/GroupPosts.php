<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupPosts extends Model
{
    protected $fillable = [
      'group_id','content','owner_id','owner_type',
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class , 'group_post_id' , 'id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class , 'group_post_id' , 'id');
    }

}
