<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ( auth('teacher')->check()  || auth('student')->check() );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|string',
            'type'    => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'content' => __('validation.attributes.content'),
            'type' => __('validation.attributes.type'),
        ];
    }
}
