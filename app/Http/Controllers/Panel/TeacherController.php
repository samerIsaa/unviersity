<?php

namespace App\Http\Controllers\Panel;

use App\Constants\StatusCodes;
use App\Http\Requests\TeacherRequest;
use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class TeacherController extends Controller
{
    protected $storagePath = 'teachers';

    public function index()
    {
        return view('panel.teachers.index');
    }


    public function create()
    {
        return view('panel.teachers.create');
    }


    public function store(TeacherRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        if ($file = $request->file('avatar')){
            $data['avatar'] = $file->store($this->storagePath);
        }


        Teacher::create($data);

        return response()->json([
            'msg' => 'تمت عملية الإضافة بنجاح'
        ], StatusCodes::OK);

    }

    public function edit($id)
    {
        $data['teacher'] = Teacher::findOrFail($id);
        return view('panel.teachers.edit', $data);
    }


    public function update(TeacherRequest $request, $id)
    {
        $teacher = Teacher::findOrFail($id);
        $data = $request->all();

        if (!$request->filled('password')) {
            $data['password'] = $teacher->password;
        }else{
            $data['password'] = Hash::make($data['password']);
        }

        if ($file = $request->file('avatar')){
            $data['avatar'] = $file->store($this->storagePath);
            Storage::delete($teacher->avatar);
        }

        $teacher->update($data);

        return response()->json([
            'msg' => 'تمت عملية التعديل بنجاح'
        ], 200);
    }


    public function destroy($id)
    {
        try {
            $admin = Teacher::destroy($id);

            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

        $search = $query['generalSearch'];


        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }
        $items = Teacher::query();


        if ($search != null) {
            $items->filter($search);
        }

        if ($sort && count($sort)) {
            $items->orderBy($sort['field'], $sort['sort']);
        } else {
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }

}
