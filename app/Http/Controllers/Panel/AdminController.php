<?php

namespace App\Http\Controllers\Panel;

use App\Admin;
use App\Constants\StatusCodes;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index()
    {
        return view('panel.admins.index');
    }


    public function create()
    {
        return view('panel.admins.create');
    }


    public function store(AdminRequest $request)
    {
        $data = $request->all();

        $data['password'] = Hash::make($data['password']);

        Admin::create($data);

        return response()->json([
            'msg' => 'تمت عملية الإضافة بنجاح'
        ], StatusCodes::OK);

    }

    public function edit($id)
    {
        $data['admin'] = Admin::findOrFail($id);
        return view('panel.admins.edit', $data);
    }


    public function update(AdminRequest $request, $id)
    {
        $admin = Admin::findOrFail($id);

        $data = $request->all();

        if (!$request->filled('password')) {
            unset($data['password']);
        }else{
            $data['password'] = Hash::make($data['password']);

        }

        $admin->update($data);

        return response()->json([
            'msg' => 'تمت عملية التعديل بنجاح'
        ], 200);
    }


    public function destroy($id)
    {
        try {
            $admin = Admin::destroy($id);

            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

        $search = $query['generalSearch'];


        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }
        $items = Admin::query();


        if ($search != null) {
            $items->filter($search);
        }

        if ($sort && count($sort)) {
            $items->orderBy($sort['field'], $sort['sort']);
        } else {
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }

}
