<?php

namespace App\Http\Controllers\Panel;


use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Support\Facades\Input;

class StudentController extends Controller
{

    public function index()
    {
        return view('panel.students.index');
    }

    public function joinRequest()
    {
        Student::whereNull('read_at')->update([
            'read_at' => now()
        ]);
        return view('panel.students.joinRequest');
    }

    public function destroy($id)
    {
        try {

            Student::destroy($id);
            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

        $search = $query['generalSearch'];


        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }
        $items = Student::query()->where('is_accepted' , 1);


        if ($search != null) {
            $items->filter($search);
        }

        if ($sort && count($sort)) {
            $items->orderBy($sort['field'], $sort['sort']);
        } else {
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }

    public function joinRequestDatatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

        $search = $query['generalSearch'];


        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }

        $items = Student::query()->where('is_accepted' , 0);


        if ($search != null) {
            $items->filter($search);
        }

        if ($sort && count($sort)) {
            $items->orderBy($sort['field'], $sort['sort']);
        } else {
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }

    public function changeBlockStatus($id)
    {
        $std = Student::find($id);
        $std->is_blocked = !($std->is_blocked);
        $std->save();
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ]);
    }

    public function changeAcceptedStatus($id)
    {
        $std = Student::find($id);
        $std->is_accepted = !($std->is_accepted);
        $std->save();
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ]);
    }
}
