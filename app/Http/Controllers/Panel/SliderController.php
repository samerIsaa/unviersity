<?php

namespace App\Http\Controllers\Panel;

use App\Http\Requests\SliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;

class SliderController extends Controller
{

    protected $storagePath = "sliders";
    public function index()
    {
        $sliders = Slider::all();
        return view('panel.slider.index' , compact('sliders'));
    }


    public function create()
    {
        return view('panel.slider.create');
    }


    public function store(SliderRequest $request)
    {
        $data = $request->all();
        if ($request->has('isVisible')){
            $data['isVisible'] =   $request->get('isVisible') == "on" ? true : false;
        }

        if ($file = $request->file('image')) {
            $data['image'] = $file->store($this->storagePath);
        }

        Slider::create($data);
        session()->flash('success', 'تمت العملية بنجاح');
        return back();
    }


    public function destroy($id , Request $request)
    {
        if ($request->json()){
            try{
                $slider = Slider::findOrFail($id);
                if ($slider ->image) {
                    Storage::delete($slider ->image);
                }
                $slider ->delete();

                return response()->json([
                    'status'    => 200 ,
                    'msg'       => 'تم حذف السلايدر بنجاح'
                ],200);
            }catch (\Exception $exception){
                return response()->json([
                    'status'    => 500 ,
                    'msg'       => 'لقد حدث خطأ ما'
                ] , 500);
            }
        }
    }


    public function updateVisiblity($id , Request $request)
    {
        if ($request->json()){


            try{
                $slider = Slider::findOrFail($id);
                $slider->update(['isVisible' => $request->visible]);

                return response()->json([
                    'status'    => 200 ,
                    'msg'       => 'تم تعديل السلايدر بنجاح'
                ],200);
            }catch (\Exception $exception){
                return response()->json([
                    'status'    => 500 ,
                    'msg'       => 'لقد حدث خطأ ما'
                ] , 500);
            }
        }
    }
}
