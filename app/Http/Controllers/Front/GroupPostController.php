<?php

namespace App\Http\Controllers\Front;

use App\Comment;
use App\Group;
use App\GroupPosts;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GroupPostController extends Controller
{

    protected $path = 'posts';
    public function create($id)
    {
        $data['group'] = Group::findOrFail($id);

        return view('front.teacher.groupPosts.create', $data);
    }

    public function store($id , Request $request)
    {
        $group = Group::findOrFail($id);
        $data = $request->all();

        $data['owner_type'] = get_class($request->user());
        $data['owner_id'] = $request->user()->id ;
        $data['group_id'] = $group->id ;

        $validator = Validator::make($data , [
            'content'  => 'required|string',
            'image'    => 'image'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $post = GroupPosts::create($data);

        if ($file = $request->file('image')){
            $dataImage['path'] = $file->store($this->path);
            $dataImage['group_post_id'] = $post->id;

            Photo::create($dataImage);
        }

        return back()->with('success' , 'تمت العملية بنجاح');

    }

    public function show($id , $post_id)
    {
        $data['post'] = GroupPosts::findOrFail($post_id);
        return view('front.teacher.groupPosts.show' ,$data);
    }

    public function storeReplay(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data , [
            'replay'  => 'required|string',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $data['group_post_id'] = $request->post_id;
        $data['content'] = $request->replay;
        $data['owner_type'] = get_class($request->user());
        $data['owner_id'] = $request->user()->id ;
        Comment::create($data);

        return back()->with('success' , 'تمت العملية بنجاح');
    }

//
//    public function destroy($id , $file_id)
//    {
//
//        try {
//            GroupFiles::destroy($file_id);
//
//            return response()->json([
//                'status' => 200,
//                'msg' => 'لقد تمت عملية الحذف بنجاح'
//            ], 200);
//
//        } catch (\Exception $exception) {
//            return response()->json([
//                'status' => 500,
//                'msg' => 'لقد حدث خطأ ما'
//            ], 500);
//        }
//    }
}
