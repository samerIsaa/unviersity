<?php

namespace App\Http\Controllers\Front;

use App\BookRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookRequestController extends Controller
{

    public function index()
    {
        $data['books'] = BookRequest::query()->orderByDesc('created_at')->paginate(10);

        return view('front.main.booksRequest.index' , $data);
    }

    public function create()
    {
        return view('front.main.booksRequest.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['owner_id'] = $request->user()->id;
        $data['owner_type'] = get_class($request->user());

        BookRequest::create($data);

        return back()->with('success' , 'تمت العملية بنجاح');
    }

}
