<?php

namespace App\Http\Controllers\Front\Teacher;

use App\Group;
use App\GroupFiles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GroupFileController extends Controller
{

    protected $path = 'files';

    public function create($id)
    {
        $data['group'] = Group::findOrFail($id);

        return view('front.teacher.groupFiles.create', $data);
    }

    public function store($id , Request $request)
    {
        $group = Group::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data , [
            'file'  => 'required|file'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }


        if ($file = $request->file('file')){
            $data['path'] = $file->store($this->path);
            $path = storage_path("app/" .$data['path']);

            $data['mime'] = File::mimeType($path);
            $data['size'] = File::size($path);
            $data['name'] = $file->getClientOriginalName();
            $data['group_id'] = $group->id;
            GroupFiles::create($data);
            return back()->with('success' , 'تمت العملية بنجاح');
        }

        return back()->with('error' , 'لقد حدث خطأ ما');

    }

    public function destroy($id , $file_id)
    {

        try {
            GroupFiles::destroy($file_id);

            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }

    public function download($id , $file_id)
    {
        $file = GroupFiles::findOrFail($file_id);


        $path = storage_path("app/".$file->path);

        return response()->download($path);

    }
}
