<?php

namespace App\Http\Controllers\Front\Teacher;

use App\Admin;
use App\Group;
use App\GroupStudent;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupStudentController extends Controller
{
    public function datatable($id)
    {
        $group = Group::findOrFail($id);
        $items = $group->students();

        return getDataTable($items);
    }

    public function create($id)
    {
        $data['group'] = Group::findOrFail($id);
        $data['students'] = Student::all();
        return view('front.teacher.groupStudents.create', $data);
    }

    public function store($id , Request $request)
    {

        $group = Group::findOrFail($id);


        if (isset($request->students) && count($request->students) > 0 ){
            foreach ($request->students as $item) {
                $std = Student::find(['id' => $item]);
                if (!isset($std)) {
                    return redirect()->back()->with('error' , 'لقد حدث خطأ ما');
                }else{
                    if (!$group->students()->where('id' , $item)->first()){
                        $group->students()->attach($std);
                    }
                }

            }
        }

        return back()->with('success' , 'تمت العملية بنجاح');
    }

    public function destroy($id , $std_id)
    {
        try {
            GroupStudent::where('group_id' , $id)
                        ->where('student_id' , $std_id)
                        ->delete();

            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }
}
