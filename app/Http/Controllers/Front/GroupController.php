<?php

namespace App\Http\Controllers\Front;

use App\Group;
use App\Http\Requests\GroupRequest;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{

    public function index()
    {
        $data['groups'] = auth('teacher')->user()->groups;

        return view('front.teacher.groups.index' ,$data);
    }

    public function indexForStudent()
    {

        $data['groups'] = auth('student')->user()->groups;

        return view('front.teacher.groups.index' ,$data);

    }
    public function create()
    {
        return view('front.teacher.groups.create');
    }

    public function store(GroupRequest $request)
    {
        $data = $request->all();
        $data['teacher_id'] = auth('teacher')->id();
        do{
            $data['code'] = rand('10000' , '99999');
        }while(Group::where('code' , $data['code'])->first());

        Group::create($data);


        return back()->with('success' , 'تم انشاء الفصل بانتظار موافقة الإدارة');
    }

    public function show($id)
    {
        $data['group'] = Group::findOrFail($id);
        $data['files'] = $data['group']->files()->paginate(2);

        return view('front.teacher.groups.show', $data);
    }
}
