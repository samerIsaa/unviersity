<?php

namespace App\Http\Controllers\Front\Student;

use App\Catagory;
use App\Course;
use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index()
    {
        return view('front.main.index');
    }
}
