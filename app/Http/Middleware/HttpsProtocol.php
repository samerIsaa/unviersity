<?php

namespace App\Http\Middleware;

use Closure;

class HttpsProtocol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (strpos(url('/'), 'www'))
            return redirect()->away('https://kaf.testarapeak.com'.$request->getRequestUri(), 301);
        if (!$request->secure())
            return redirect()->secure($request->getRequestUri(), 301);

        return $next($request);
    }
}
