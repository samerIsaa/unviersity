<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookRequest extends Model
{
    protected $fillable = [
        'owner_id' , 'owner_type' , 'content' , 'type'
    ];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class , 'owner_id' , 'id');
    }
    public function student()
    {
        return $this->belongsTo(Teacher::class , 'owner_id' , 'id');
    }

    public function ownerData()
    {
        switch ($this->owner_type){
            case 'App\Student' : return $this->student; break;
            case 'App\Teacher' : return $this->teacher; break;
        }
    }



}
