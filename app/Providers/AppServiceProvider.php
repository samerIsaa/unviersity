<?php

namespace App\Providers;

use App\Student;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('panel.layout.master', function ($view) {
            $data['newJoinRequest'] = Student::query()->where('read_at', null)->count();
            $view->with($data);
        });
    }
}
