<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    protected $fillable = [
        'name' , 'email' , 'u_id' , 'password' , 'avatar' , 'is_accepted' , 'is_blocked' , 'read_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeFilter($q , $search)
    {
        return $q->where('name' , 'like' , '%'.$search.'%')
            ->orWhere('email' , 'like' , '%'.$search.'%')
            ->orWhere('mobile' , 'like' , '%'.$search.'%');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class , 'group_students');
    }
}
