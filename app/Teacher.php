<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Teacher extends Authenticatable
{
    protected $fillable = [
        'name' , 'email' , 'mobile' , 'password' , 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeFilter($q , $search)
    {
        return $q->where('name' , 'like' , '%'.$search.'%')
            ->orWhere('email' , 'like' , '%'.$search.'%')
            ->orWhere('mobile' , 'like' , '%'.$search.'%');
    }


    public function groups()
    {
        return $this->hasMany(Group::class)->where('is_accepted' , 1);
    }
}
