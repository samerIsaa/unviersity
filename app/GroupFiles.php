<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupFiles extends Model
{
    protected $fillable = [
        'group_id' ,'name', 'path' , 'mime' , 'size'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }


}



