<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
      'group_post_id','owner_id','owner_type','content'
    ];

    protected $appends = ['owner_name'];

    public function post()
    {
        return $this->belongsTo(GroupPosts::class , 'group_post_id' , 'id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class , 'owner_id' , 'id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class , 'owner_id' , 'id');
    }

    public function getOwnerNameAttribute()
    {
        switch ($this->owner_type ){
            case 'App\Teacher' : return $this->teacher->name;break;
            case 'App\Student' : return $this->student->name;break;
        }
    }
}
