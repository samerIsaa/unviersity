<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'group_post_id' , 'path'
    ];


    public function groupPost()
    {
        return $this->belongsTo(GroupPosts::class , 'group_post_id' , 'id');
    }
}
