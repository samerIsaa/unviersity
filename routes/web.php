<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('student.auth.login');
});
Route::get('image/{folder}/{path}/{size?}', 'Controller@photo');



Route::group(['middleware'  => 'auth:student,teacher'], function () {




    Route::group(['prefix'  => 'group/{id}' ,'as' => 'groups.' ,  'namespace' => 'Front'], function () {
        Route::get('/' , ['as' => 'show' , 'uses' => 'GroupController@show']);


        Route::group(['prefix'  => 'posts' , 'as' => 'posts.'], function () {

            Route::group(['prefix'  => 'add' ], function () {
                Route::get('/' , ['as' => 'add' , 'uses' => 'GroupPostController@create']);
                Route::post('/', ['as' => 'add' , 'uses' => 'GroupPostController@store']);
            });

            Route::get('{post_id}' , ['as' => 'show' , 'uses' => 'GroupPostController@show'] );
            Route::delete('{post_id}' , ['as' => 'destroy' , 'uses' => 'GroupPostController@destroy'] );


            Route::group(['prefix'  => 'replies' , 'as' => 'replies.' ], function () {
                Route::post('/store', ['as' => 'store' , 'uses' => 'GroupPostController@storeReplay']);
            });

        });

    });




    Route::group(['prefix'  => 'books' , 'as' => 'books.', 'namespace' => 'Front'], function () {
        Route::get('/' , ['as' => 'index' , 'uses' => 'BookRequestController@index']);


        Route::group(['prefix'  => 'create' ], function () {
            Route::get('/' , ['as' => 'create' , 'uses' => 'BookRequestController@create']);
            Route::post('/', ['as' => 'store' , 'uses' => 'BookRequestController@store']);
        });

    });






});
