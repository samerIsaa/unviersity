<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'student' , 'as' => 'student.' ], function () {

    Route::group(['as' => 'auth.', 'namespace' => 'Student\Auth'], function () {

        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');

        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');

        Route::get('logout', 'LoginController@logout')->name('logout');

    });


    Route::group(['middleware'  => 'auth:student' , 'namespace' => 'Student'], function () {

        Route::get('/' , ['as' => 'index' , 'uses' => 'HomeController@index']);

    });

    Route::group(['prefix'  => 'group' , 'as' => 'groups.' , 'middleware'  => 'auth:student'], function () {

        Route::get('/' , ['as' => 'index' , 'uses' => 'GroupController@indexForStudent']);


    });




});

