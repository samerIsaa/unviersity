<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'teacher' , 'as' => 'teacher.'], function () {

    Route::group(['as' => 'auth.', 'namespace' => 'Teacher\Auth'], function () {

        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');

        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');

        Route::get('logout', 'LoginController@logout')->name('logout');

    });



    Route::group(['middleware'  => 'auth:teacher', 'namespace' => 'Teacher'], function () {

        Route::get('/' , ['as' => 'index' , 'uses' => 'HomeController@index']);

    });


    Route::group(['prefix'  => 'group' , 'as' => 'groups.' , 'middleware'  => 'auth:teacher'], function () {

        Route::get('/' , ['as' => 'index' , 'uses' => 'GroupController@index']);

        Route::group(['prefix'  => 'create' ], function () {
            Route::get('/' , ['as' => 'create' , 'uses' => 'GroupController@create']);
            Route::post('/', ['as' => 'store' , 'uses' => 'GroupController@store']);
        });

        Route::group(['prefix'  => '{id}' ], function () {


            Route::group(['namespace' => 'Teacher'], function () {
                Route::group(['prefix'  => 'students' , 'as' => 'students.' ], function () {

                    Route::group(['prefix'  => 'add' ], function () {
                        Route::get('/' , ['as' => 'add' , 'uses' => 'GroupStudentController@create']);
                        Route::post('/', ['as' => 'add' , 'uses' => 'GroupStudentController@store']);
                    });

                    Route::get('/datatable' , 'GroupStudentController@datatable' );
                    Route::delete('/remove/{std_id}' , 'GroupStudentController@destroy' );
                });

                Route::group(['prefix'  => 'files' , 'as' => 'files.'], function () {

                    Route::group(['prefix'  => 'add' ], function () {
                        Route::get('/' , ['as' => 'add' , 'uses' => 'GroupFileController@create']);
                        Route::post('/', ['as' => 'add' , 'uses' => 'GroupFileController@store']);
                    });

                    Route::delete('/remove/{file_id}' , ['as' => 'remove' , 'uses' => 'GroupFileController@destroy'] );
                    Route::get('/download/{file_id}' , ['as' => 'download' , 'uses' => 'GroupFileController@download'] );
                });
            });

        });
    });




});

