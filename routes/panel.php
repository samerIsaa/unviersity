<?php


Route::group(['prefix' => 'panel', 'as' => 'panel.'], function () {

    Route::get('/' , function (){
        return redirect(route('panel.showLogin'));
    });

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('showLogin');
    Route::post('login', 'Auth\LoginController@login')->name('login');

    /*
     * Reset Password Routes
     */
    Route::group(['prefix' => 'password/'], function () {
        Route::post('email' , 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('reset/{token}' , 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('reset' , 'Auth\ResetPasswordController@reset')->name('password.update');
    });


    Route::group(['middleware' => 'auth:admin'], function () {

//        Route::post('upload', 'ImageController@upload')->name('index');
//        Route::post('delete', 'ImageController@delete')->name('index');


        Route::get('index' , 'HomeController@index')->name('index');


        Route::resource('admins' , 'AdminController')->except(['show']);
        Route::get('admins/datatable' , 'AdminController@datatable');

        Route::resource('teachers', 'TeacherController')->except(['show']);
        Route::get('teachers/datatable', 'TeacherController@datatable');

        Route::group(['prefix' => 'students' , 'as' => 'students.'], function () {

            Route::get('/' , ['as'=>'index' , 'uses' => 'StudentController@index']);
            Route::get('/join-request' , ['as'=>'joinRequest' , 'uses' => 'StudentController@joinRequest']);

            Route::get('datatable' , 'StudentController@datatable');
            Route::get('datatable/joinRequest' , 'StudentController@joinRequestDatatable');

            Route::group(['prefix' => '{id}'], function () {
                Route::delete('/', ['as'=> 'destroy' , 'uses' =>'StudentController@destroy']);
                Route::post('changeBlockStatus' , ['as'=> 'changeBlockStatus' , 'uses' =>'StudentController@changeBlockStatus']);
                Route::post('changeAcceptedStatus' , ['as'=> 'changeAcceptedStatus' , 'uses' =>'StudentController@changeAcceptedStatus']);
            });
        });


//        Route::resource('catagories' , 'CatagoryController')->except(['create' , 'edit' , 'show']);
//        Route::get('catagories/datatable' , 'CatagoryController@datatable');
//
//
//        Route::resource('courses' , 'CourseController')->except(['show']);
//        Route::group(['prefix' => 'courses' , 'as'  => 'courses.'], function () {
//            Route::get('/datatable' , 'CourseController@datatable');
//            Route::get('{id}/lectures' , 'LectureController@index');
//
//            Route::group(['prefix' => '{course_id}/lectures' , 'as'  => 'lectures.'], function () {
//                Route::get('/' , 'LectureController@index')->name('index');
//                Route::get('/create' , 'LectureController@create')->name('create');
//                Route::post('/' , 'LectureController@store')->name('store');
//                Route::get('/{id}/edit' , 'LectureController@editCourseSection');
//                Route::put('/{id}' , 'LectureController@updateCourseSection')->name('updateCourseSection');
//                Route::delete('/{id}' , 'LectureController@deleteCourseSection');
//                Route::get('/datatable' , 'LectureController@datatable');
//
//
//            });
//        });

        Route::resource('settings' , 'SettingsController')->except(['create' , 'edit' , 'show' , 'destroy']);

        Route::resource('slider' , 'SliderController')->except(['show' , 'edit' , 'update' ]);
        Route::post('slider/update-visiblity/{id}' , 'SliderController@updateVisiblity')->name('slider.updateVisiblity');





        Route::get('logout' , function (){
           \Illuminate\Support\Facades\Auth::guard('admin')->logout();
           return back();
        })->name('logout');
    });


});
