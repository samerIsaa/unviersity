@extends('front.layout.master' , ['title' => 'طلبات الكتب'])

@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> طلبات الكتب </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')

    <!--begin: Row -->
    @if($books)

        @foreach($books as $book)
            <div class="row">

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head p-3">
                        <div class="col-md-4">{{ $book->ownerData()->name }}</div>
                        <div class="col-md-4 text-center">
                            <h3 class="text-dark">{{ $book->type == 0 ? 'طلب' : 'عرض' }}</h3>
                        </div>
                        <div class="col-md-4 text-right">{{ $book->created_at->format('Y-m-d g:i A') }}</div>
                    </div>

                    <div class="kt-portlet__body">
                        {!! $book->content !!}
                    </div>
                </div>

            </div>

        @endforeach
    @endif


    <!--end: Row -->


@endsection
