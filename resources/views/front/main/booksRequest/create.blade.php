@extends('front.layout.master' , ['title' => 'طلب/عرض كتاب'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> طلب/عرض كتاب </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')


    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            طلب/عرض كتاب
                        </h3>
                    </div>
                </div>


                <!--begin::Form-->
                <form class="kt-form" action="{{ route('books.store') }}" method="post">
                    @csrf
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">{{ $errors->first() }}</div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif
                        <div class="form-group">
                            <label>محتوى الكتاب</label>
                            <textarea type="text" class="form-control" name="content"
                                      placeholder="محتوى الكتاب">{{ old('name') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>نوع الطلب</label>
                            <select class="form-control bootstrap-select" name="type">
                                <option value="0">طلب كتاب</option>
                                <option value="1">عرض كتاب</option>
                            </select>
                        </div>

                    </div>

                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>


    @push('js')
        <script>
            $('.bootstrap-select').selectpicker();
        </script>
    @endpush
@endsection
