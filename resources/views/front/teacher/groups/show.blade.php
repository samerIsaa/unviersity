@extends('front.layout.master', ['title' => $group->name ])


@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> الفصول الدراسية </a>
                </div>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> {{ $group->name }} </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')

    <div class="row">
        <div class="col-xl-12">


            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">


                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line nav-tabs-line-right nav-tabs-line-brand"
                            role="tablist">

                            @if (auth('teacher')->check())
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_2_1" role="tab">
                                        <i class="la la-user"></i>
                                        الطلاب
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_2_2" role="tab">
                                        <i class="la la-file"></i>
                                        الملفات
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_2_3" role="tab">
                                        <i class="la la-newspaper-o"></i>
                                        الأخبار
                                    </a>
                                </li>
                            @else

                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_2_3" role="tab">
                                        <i class="la la-newspaper-o"></i>
                                        الأخبار
                                    </a>
                                </li>

                            @endif




                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">

                        @if (auth('teacher')->check())

                            <div class="tab-pane active" id="kt_portlet_tab_2_1">

                                <div class="kt-portlet__head-toolbar mb-3">
                                    <div class="kt-portlet__head-wrapper">
                                        <div class="kt-portlet__head-actions">
                                            <a href="{{ route('teacher.groups.students.add' , $group->id) }}"
                                               class="btn btn-brand btn-elevate btn-icon-sm">
                                                <i class="la la-plus"></i>
                                                إضافة طالب
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <!--begin: Datatable -->
                                <div class="kt-datatable text-center" id="ajax_data"></div>

                                <!--end: Datatable -->

                            </div>

                            <div class="tab-pane " id="kt_portlet_tab_2_2">


                                <div class="kt-portlet__head-toolbar mb-3">
                                    <div class="kt-portlet__head-wrapper">
                                        <div class="kt-portlet__head-actions">
                                            <a href="{{ route('teacher.groups.files.add' , $group->id) }}"
                                               class="btn btn-brand btn-elevate btn-icon-sm">
                                                <i class="la la-upload"></i>
                                                رفع ملف جديد
                                            </a>
                                        </div>
                                    </div>
                                </div>


                                <table class="table text-center">
                                    <thead>

                                    <tr>
                                        <th scope="col">اسم الملف</th>
                                        <th scope="col">الحجم</th>
                                        <th scope="col">تاريخ النشر</th>
                                        <th scope="col">العمليات</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if($files)
                                        @foreach($files as $file)
                                            <tr>
                                                <th>{{ $file->name }}</th>
                                                <td>{{ human_filesize($file->size) }}</td>
                                                <td>{{ $file->created_at->format('Y-m-d g:i A') }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                                            <i class="la la-cog"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('teacher.groups.files.download', ['id' => $group->id , 'file_id' => $file->id]) }}"><i class="la la-download"></i> تحميل </a>
                                                            <a class="dropdown-item delete" href="#" data-url="{{ route('teacher.groups.files.remove' , ['id' => $group->id , 'file_id' => $file->id]) }}">
                                                                <i class="la la-times"></i> حذف
                                                            </a>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>

                                        @endforeach
                                    @endif


                                    </tbody>
                                </table>


                            </div>
                        @endif


                        <div class="tab-pane @if (auth('student')->check()) active @endif " id="kt_portlet_tab_2_3">


                            <div class="kt-portlet__head-toolbar mb-3">
                                <div class="kt-portlet__head-wrapper">
                                    <div class="kt-portlet__head-actions">
                                        <a href="{{ route('groups.posts.add' , $group->id) }}"
                                           class="btn btn-brand btn-elevate btn-icon-sm">
                                            <i class="la la-plus"></i>
                                            نشر خبر
                                        </a>
                                    </div>
                                </div>
                            </div>


                            @if($group->posts->count())
                                @foreach($group->posts()->orderByDesc('created_at')->get() as $post)
                                    <div class="kt-portlet kt-portlet--mobile">
                                        @if ($post->photos->count())
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label p-3 mx-auto">
                                                <img src="{{ url('image/' . $post->photos[0]->path . '/300x300' ) }}" alt="">
                                            </div>
                                        </div>
                                        @endif
                                        <div class="kt-portlet__body">
                                            {!! $post->content !!}
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <a href="{{ route('groups.posts.show' , ['id' => $group->id , 'post_id' =>$post->id ]) }}"
                                               class="btn btn-brand btn-elevate btn-icon-sm">
                                                مشاهدة الخبر
                                            </a>
                                        </div>
                                    </div>

                                @endforeach
                            @endif



                        </div>
                    </div>
                </div>
            </div>

            <!--end::Portlet-->


        </div>
    </div>
@endsection
@push('js')
    <script src="{{asset('panelAssets/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>



    <script>


        window.data_url = '{{url('teacher/group/'. $group->id .'/students/datatable')}}';

        window.columns = [
            {
                field: 'name',
                title: "الإسم",
                textAlign: "center",
            }, {
                field: 'email',
                title: "البريد الإلكتروني",
                textAlign: "center",

            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                width: 100,
                template: function (data) {
                    var url = "{{ url('teacher/group/'. $group->id .'/students/remove') }}/" + data.id;
                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item delete" href="#" data-url="` + url + `"><i class="la la-times"></i> حذف </a>
						  	</div>
						</div>`;
                },
            }
        ];


    </script>

@endpush
