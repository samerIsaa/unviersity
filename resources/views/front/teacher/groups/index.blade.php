@extends('front.layout.master' , ['title' => 'الفصول الدراسية'])

@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> الفصول الدراسية </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')

    <!--begin: Row -->
    <div class="row">
        @if($groups)

            @foreach($groups as $group)
                <div class="col-lg-4">
                    <div class="kt-portlet">
                        <a href="{{ route('groups.show' , $group->id) }}">
                            <div class="kt-portlet__head kt-ribbon kt-ribbon--right">
                                <div class="kt-ribbon__target" style="top: 10px; left: -2px;">{{ $group->code }}</div>
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        {{ $group->name }}
                                    </h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        @endif

    </div>

    <!--end: Row -->


@endsection
