@extends('front.layout.master' , ['title' => 'رفع ملف'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('panelAssets/js/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css"/>

@endpush
@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('groups.show' , $group->id) }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('groups.show' , $group->id) }}" class="kt-subheader__breadcrumbs-link"> {{ $group->name }} </a>
                </div>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> رفع ملف </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            رفع ملف
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--fit kt-form--label-right" action="{{ route('teacher.groups.files.add' , $group->id) }}"
                            method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                                <div class="alert alert-danger">{{ $errors->first() }}</div>
                        @endif
                        @if ( session('error') )
                                <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif

                            <div class="form-group">
                                <label>رفع ملف</label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="file" id="customFile">
                                    <label class="custom-file-label" style="text-align: right!important;" for="customFile">رفع ملف</label>
                                </div>
                            </div>



                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </div>
                </form>

                <!--end::Portlet-->


            </div>
        </div>
    </div>

@endsection

