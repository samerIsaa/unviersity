<!DOCTYPE html>

<html direction="rtl" dir="rtl" style="direction: rtl">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>تسجيل الدخول</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">


    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('panelAssets/css/login-3.rtl.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('panelAssets/css/style.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />


    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <style>
        *{
            font-family: 'Cairo', sans-serif;
        }
    </style>
</head>

<!-- end::Head -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"  style="background-image: url({{ asset('frontAssets/media/bg.jpg') }});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{ asset('frontAssets/media/sitelogo.jpg') }}" width="400" height="200">
                        </a>
                    </div>

                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">تسجيل الدخول</h3>
                        </div>

                        <div class="row">

                            <div class="form-group d-flex col-lg-12 mx-auto">
                                <a class="form-control text-center" href="{{ route('student.auth.login') }}"><strong>الدخول كطالب</strong></a>
                                <a class="form-control text-center btn-brand mx-2" href="{{ route('teacher.auth.login') }}"><strong>الدخول
                                        كمعلم</strong></a>
                            </div>
                        </div>
                        <form class="kt-form" action="{{ route('teacher.auth.login') }}" method="post">
                            @csrf
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="إسم المستخدم" name="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="كلمة المرور" name="password">
                            </div>

                            <div class="row kt-login__extra">
                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="remember"> تذكرني
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col kt-align-right">
                                    <a href="{{ route('teacher.auth.register') }}" class="kt-link kt-login__link">تسجيل جديد؟</a>
                                </div>
                            </div>

                            <div class="kt-login__actions">
                                <button class="btn btn-brand btn-elevate kt-login__btn-primary">تسجيل الدخول</button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#22b9ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->

<!--begin:: Vendor Plugins -->
<script src="{{ asset('panelAssets') }}//js/jquery.js" type="text/javascript"></script>

<script src="{{ asset('panelAssets') }}//js/sticky.min.js" type="text/javascript"></script>

<script src="{{ asset('panelAssets') }}//js/jquery.form.min.js" type="text/javascript"></script>


<script src="{{ asset('panelAssets') }}//js/owl.carousel.js" type="text/javascript"></script>

<script src="{{ asset('panelAssets') }}//js/jquery.validate.js" type="text/javascript"></script>

<script src="{{ asset('panelAssets') }}//js/jquery-validation.init.js" type="text/javascript"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!--end:: Vendor Plugins -->
<script src="{{ asset('panelAssets/js/scripts.bundle.js') }}" type="text/javascript"></script>

<script src="{{ asset('panelAssets/js/login-general.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>
