@extends('front.layout.master' , ['title' => 'اضافة طالب للفصل'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('panelAssets/js/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css"/>

@endpush
@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('groups.show' , $group->id) }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('groups.show' , $group->id) }}" class="kt-subheader__breadcrumbs-link"> {{ $group->name }} </a>
                </div>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> اضافة طالب للفصل </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            اضافة طالب للفصل
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--fit kt-form--label-right" action="{{ route('teacher.groups.students.add' , $group->id) }}"
                            method="post">
                    @csrf
                    <div class="kt-portlet__body">
                        @if (session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif

                        <div class="form-group">
                            <label>اسم الفصل</label>
                            <select class="form-control kt-select2" id="kt_select2_1" name="students[]" multiple>
                                @foreach($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                                @endforeach
                            </select>
                        </div>


                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </div>
                </form>

                <!--end::Portlet-->


            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{ asset('panelAssets/js/select2/dist/js/select2.full.js') }}"></script>
    <script src="{{ asset('panelAssets/js/select2.js') }}"></script>
@endpush
