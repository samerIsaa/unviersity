@extends('front.layout.master', ['title' => 'عرض الخبر' ])



@section('subheade')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الرئيسية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('groups.show' , $post->group->id) }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('groups.show' , $post->group->id) }}" class="kt-subheader__breadcrumbs-link"> {{ $post->group->name }} </a>
                </div>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link"> الأخبار </a>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        @if ($post->photos->count())
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label p-3 mx-auto">
                    <img src="{{ url('image/' . $post->photos[0]->path . '/300x300' ) }}" alt="">
                </div>
            </div>
        @endif
        <div class="kt-portlet__body">
            {!! $post->content !!}
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            @if ($errors->any())
                <div class="alert alert-danger">{{ $errors->first() }}</div>
            @endif
            @if ( session('error') )
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif

            <form action="{{ route('groups.posts.replies.store' , $post->group_id) }}" method="post">

                @csrf
                <input type="hidden" name="post_id" value="{{ $post->id }}">
                <div class="form-group">
                    <label>الرد على الخبر</label>
                    <textarea class="form-control mt-2" rows="5" name="replay"
                              placeholder="الرد على الخبر">{{ old('replay') }}</textarea>
                </div>
                <div class="kt-portlet__foot">
                    <button type="submit" class="btn btn-brand btn-elevate btn-icon-sm">
                        ارسال رد
                    </button>
                </div>

            </form>

        </div>
    </div>

    @foreach($post->comments as $comment)

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head p-3">
                <div class="col-md-6">{{ $comment->owner_name }}</div>
                <div class="col-md-6 text-right">{{ $comment->created_at->format('Y-m-d g:i A') }}</div>
            </div>

            <div class="kt-portlet__body">
                {!! $comment->content !!}
            </div>
        </div>

    @endforeach

@endsection
@push('js')



@endpush
