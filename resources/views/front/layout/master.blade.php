<!DOCTYPE html>

<html lang="en" direction="rtl" style="direction: rtl;">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>{{ $title??'الجامعة' }}</title>

    @include('front.layout.head')
    @stack('css')
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">



<!-- begin:: Page -->

<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="{{ url('/') }}">
            <img alt="Logo" src="{{ asset('frontAssets/media/logo.jpg') }}" width="50" height="50"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                class="flaticon-more-1"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
                @include('front.layout.header')
            <!-- end:: Header -->




            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body" style="background-image: url({{ asset('frontAssets/media/bg.jpg') }});">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Subheader -->

                    @yield('subheade')

                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" >

                    @yield('content')
                </div>

                <!-- end:: Content -->
                </div>
            </div>

            <!-- begin:: Footer -->
                @include('front.layout.footer')
            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->


<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->



@include('front.layout.scripts')
@stack('js')

</body>
</html>
