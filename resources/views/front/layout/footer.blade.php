<div class="kt-footer kt-grid__item" id="kt_footer">
    <div class="kt-container ">
        <div class="kt-footer__wrapper">
            <div class="kt-footer__copyright">
                {{ now()->year }}&nbsp;&copy;&nbsp;<a href="javascript:;" target="_blank" class="kt-link">الجامعة</a>
            </div>
        </div>
    </div>
</div>
