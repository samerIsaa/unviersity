<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-container  kt-container--fluid ">

        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                class="la la-close"></i></button>
        <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">

            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                <ul class="kt-menu__nav ">

                    @if(auth('teacher')->check())
                        <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                            data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;"
                                                                                       class="kt-menu__link kt-menu__toggle"><span
                                    class="kt-menu__link-text">الفصول الدراسية</span><i
                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                            href="{{ route('teacher.groups.create') }}"
                                            class="kt-menu__link "><i
                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                class="kt-menu__link-text">انشاء صف</span></a>
                                    </li>
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                            href="{{ route('teacher.groups.index') }}"
                                            class="kt-menu__link "><i
                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                class="kt-menu__link-text">جميع الصفوف</span></a>
                                    </li>


                                </ul>
                            </div>
                        </li>

                    @elseif(auth('student')->check())
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="{{ route('student.groups.index') }}"
                                class="kt-menu__link ">
                                <span class="kt-menu__link-text">جميع الصفوف</span>
                            </a>
                        </li>
                    @endif


                        <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                            data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;"
                                                                                       class="kt-menu__link kt-menu__toggle"><span
                                    class="kt-menu__link-text">الكتب</span><i
                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                            href="{{ route('books.create') }}"
                                            class="kt-menu__link "><i
                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                class="kt-menu__link-text">عرض/طلب كتاب</span></a>
                                    </li>
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                            href="{{ route('books.index') }}"
                                            class="kt-menu__link "><i
                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                class="kt-menu__link-text">جميع الكتب</span></a>
                                    </li>


                                </ul>
                            </div>
                        </li>


                </ul>
            </div>
        </div>

        <!-- end: Header Menu -->

        <!-- begin:: Brand -->
        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
            <a class="kt-header__brand-logo" href="?page=index">
                <img alt="Logo" src="{{ asset('frontAssets/media/logo.jpg') }}" width="100" height="80px"/>
            </a>
        </div>

        <!-- end:: Brand -->

        <!-- begin:: Header Topbar -->
        <div class="kt-header__topbar kt-grid__item">


            <!--begin: Notifications -->
{{--            <div class="kt-header__topbar-item dropdown">--}}

{{--                <div class="kt-header__topbar-wrapper">--}}
{{--                                <span class="kt-header__topbar-icon">--}}
{{--                                    <a href="{{ url('/') }}">--}}
{{--                                        <i class="flaticon2-bell-alarm-symbol"></i>--}}
{{--                                    </a>--}}
{{--                                </span>--}}
{{--                    <span class="kt-badge kt-badge--danger"></span>--}}
{{--                </div>--}}


{{--            </div>--}}

            <!--end: Notifications -->


            <!--begin: User bar -->
            @if (auth('teacher')->check())

                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                        <span class="kt-header__topbar-welcome kt-visible-desktop">مرحبا,</span>
                        <span class="kt-header__topbar-username kt-visible-desktop">{{ auth('teacher')->user()->name }}</span>
                        <img alt="Pic" src="{{ asset('panelAssets/media/users/default.jpg') }}"/>
                    </div>
                    <div
                        class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                            <div class="kt-user-card__avatar">
                                <img class="kt-hidden-" alt="Pic"
                                     src="{{ asset('panelAssets/media/users/default.jpg') }}"/>
                            </div>
                            <div class="kt-user-card__name">
                                {{ auth('teacher')->user()->name }}
                            </div>
                        </div>

                        <!--end: Head -->

                        <!--begin: Navigation -->
                        <div class="kt-notification">
{{--                            <a href="#"--}}
{{--                               class="kt-notification__item">--}}
{{--                                <div class="kt-notification__item-icon">--}}
{{--                                    <i class="flaticon2-calendar-3 kt-font-success"></i>--}}
{{--                                </div>--}}
{{--                                <div class="kt-notification__item-details">--}}
{{--                                    <div class="kt-notification__item-title kt-font-bold">--}}
{{--                                        My Profile--}}
{{--                                    </div>--}}
{{--                                    <div class="kt-notification__item-time">--}}
{{--                                        Account settings and more--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
                            <div class="kt-notification__custom kt-space-between">
                                <a href="{{ route('teacher.auth.logout') }}"
                                   class="btn btn-label btn-label-brand btn-sm btn-bold">تسجيل الخروج</a>

                            </div>
                        </div>

                        <!--end: Navigation -->
                    </div>
                </div>

            @elseif(auth('student')->check())
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                        <span class="kt-header__topbar-welcome kt-visible-desktop">مرحبا,</span>
                        <span class="kt-header__topbar-username kt-visible-desktop">{{ auth('student')->user()->name }}</span>
                        <img alt="Pic" src="{{ asset('panelAssets/media/users/default.jpg') }}"/>
                    </div>
                    <div
                        class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                            <div class="kt-user-card__avatar">
                                <img class="kt-hidden-" alt="Pic"
                                     src="{{ asset('panelAssets/media/users/default.jpg') }}"/>
                            </div>
                            <div class="kt-user-card__name">
                                {{ auth('student')->user()->name }}
                            </div>
                        </div>

                        <!--end: Head -->

                        <!--begin: Navigation -->
                        <div class="kt-notification">
{{--                            <a href="#"--}}
{{--                               class="kt-notification__item">--}}
{{--                                <div class="kt-notification__item-icon">--}}
{{--                                    <i class="flaticon2-calendar-3 kt-font-success"></i>--}}
{{--                                </div>--}}
{{--                                <div class="kt-notification__item-details">--}}
{{--                                    <div class="kt-notification__item-title kt-font-bold">--}}
{{--                                        My Profile--}}
{{--                                    </div>--}}
{{--                                    <div class="kt-notification__item-time">--}}
{{--                                        Account settings and more--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
                            <div class="kt-notification__custom kt-space-between">
                                <a href="{{ route('student.auth.logout') }}"
                                   class="btn btn-label btn-label-brand btn-sm btn-bold">تسجيل الخروج</a>

                            </div>
                        </div>

                        <!--end: Navigation -->
                    </div>
                </div>
            @endif

        <!--end: User bar -->

        </div>

        <!-- end:: Header Topbar -->
    </div>
</div>
