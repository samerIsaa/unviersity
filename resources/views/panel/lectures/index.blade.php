@extends('panel.layout.master' , ['title' => 'المنهاج'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
@endpush


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">المنهاج</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-book"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    المنهاج
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('panel.courses.lectures.create' , $course_id) }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            إضافة
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">

                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <form id="searchForm">
                            <div class="row align-items-center">
                                <div class="col-md-3 kt-margin-b-20-tablet-and-mobile mb-3">
                                    <input type="text" class="form-control"
                                           placeholder="بحث..." id="generalSearch">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>


            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="kt-datatable text-center" id="ajax_data"></div>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('panelAssets/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>


    <script>
        $('.bootstrap-select').selectpicker();

        window.data_url = '{{url('panel/courses/' . $course_id . '/lectures/datatable')}}';

        window.columns = [
            // {
            //     field: '',
            //     title: '',
            //     width: 40,
            //     sortable: false,
            //     // type: 'number',
            //     selector: {class: 'kt-checkbox--solid', name: 'admins'},
            // },

            {
                field: 'name',
                title: "إسم القسم",
                textAlign: "center",

            },
            {
                field: 'created_at',
                title: "تاريخ الإضافة",
                textAlign: "center",
            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                width: 100,
                template: function (data) {

                    var editUrl = "{{ url('panel/courses/'.$course_id . '/lectures') }}/" + data.id + "/edit";
                    var url = "{{ url('panel/courses/'.$course_id . '/lectures') }}/" + data.id;
                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="` + editUrl + `" ><i class="la la-edit"></i> تعديل</a>
                                    <a class="dropdown-item delete" href="#" data-url="` + url + `"><i class="la la-times"></i> حذف </a>
						  	</div>
						</div>`;
                },
            }
        ];




    </script>




@endpush
