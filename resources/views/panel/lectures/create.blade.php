@extends('panel.layout.master' , ['title' => 'إضافة منهاج'])

@push('css')

@endpush

@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">إضافة منهاج</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <form class="kt-form" id="kt_form_1">
        @csrf
        <div class="row">


            <div class="col-md-9">

                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                إضافة منهاج
                            </h3>
                        </div>
                    </div>


                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>عنوان المنهاج</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="عنوان المنهاج">
                        </div>
                    </div>


                </div>

                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                الدروس
                            </h3>
                        </div>
                    </div>


                    <div class="kt-portlet__body">


                        <div id="kt_repeater_1">
                            <div class="form-group form-group-last">

                                <div data-repeater-list="lectures">
                                    <div data-repeater-item class="form-group row align-items-center">

                                        <div class="col-md-6 mb-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__control">
                                                    <input class="form-control m-input" type="text" name="title"
                                                           placeholder="العنوان" required="required">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__control">
                                                    <input class="form-control m-input" type="text" name="description"
                                                           placeholder="وصف قصير" required="required">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__control">
                                                    <input class="form-control m-input" required="required" type="url" name="link"
                                                           placeholder="رابط الدرس">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>

                                        <div class="col-md-4">
                                            <a href="javascript:;" data-repeater-delete=""
                                               class="btn-sm btn btn-label-danger btn-bold">
                                                <i class="la la-trash-o"></i>
                                                حذف
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-group-last row">
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create=""
                                       class="btn btn-bold btn-sm btn-label-success">
                                        <i class="la la-plus"></i> إضافة
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>


            </div>

            <div class="col-lg-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label" style="width: 100%;">
                            <button type="submit" style="width: 100%;" id="save" class="btn btn-brand">إضافة</button>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </form>

@endsection


@push('js')
    <script src="{{ asset('panelAssets/js/jquery.repeater/src/lib.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panelAssets/js/jquery.repeater/src/jquery.input.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panelAssets/js/jquery.repeater/src/repeater.js') }}" type="text/javascript"></script>

    <script src="{{ asset('panelAssets/js/form-repeater.js') }}" type="text/javascript"></script>

    <script>


        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $("#kt_form_1").validate({
                    // define validation rules
                    rules: {
                        name: {
                            required: true,
                        },
                    },

                    //display error alert on form submit
                    invalidHandler: function (event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            };


            return {
                // public functions
                init: function () {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });


        $('#kt_form_1').submit(function (e) {
            e.preventDefault();

            if ($(this).valid()) {
                $('#save').attr('disabled', 'disabled')
                    .html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

                var fd = new FormData(this);

                $.ajax({
                    url: "{{ route('panel.courses.lectures.store' , $course_id) }}",
                    method: 'POST',
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (response) {
                        $('#save').removeAttr('disabled').html('إضافة');
                        swal.fire({
                            type: 'success',
                            title: response.msg,
                            confirmButtonText: 'موافق'
                        }).then((result) => {
                            location.reload();
                        });
                    },
                    error: function (response) {
                        $('#save').removeAttr('disabled').html('إضافة');
                        swal.fire(response.responseJSON.msg, "", "error");

                    }
                });
            }


        })



    </script>

@endpush
