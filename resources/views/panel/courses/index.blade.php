@extends('panel.layout.master' , ['title' => 'جميع الدورات'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
@endpush


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">جميع الدورات</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-university"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    جميع الدورات
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('panel.courses.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            إضافة دورة
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">

                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <form id="searchForm">
                            <div class="row align-items-center">
                                <div class="col-md-3 kt-margin-b-20-tablet-and-mobile mb-3">
                                    <input type="text" class="form-control"
                                           placeholder="العنوان" id="title">
                                </div>

                                <div class="col-md-3 kt-margin-b-20-tablet-and-mobile mb-3">

                                    <select class="form-control bootstrap-select" name="catagory_id" id="catagory_id">
                                        <option value=" " selected>الكل</option>

                                        @if($catagories)
                                            @foreach($catagories as $catagory)
                                                <option value="{{ $catagory['id'] }}">{{ $catagory['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-6 kt-margin-b-20-tablet-and-mobile mb-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="javascript:;" id="search"
                                               class="btn btn-block btn-brand btn-elevate btn-icon-sm">
                                                <i class="la la-search"></i>
                                                البحث
                                            </a>
                                        </div>
                                        <div class="col-md-6"><a href="javascript:;" id="cancelSearch"
                                                                 class="btn btn-block btn-brand btn-elevate btn-icon-sm">
                                                <i class="flaticon-cancel"></i>
                                                إلغاء البحث
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>


            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="kt-datatable text-center" id="ajax_data"></div>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('panelAssets/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>


    <script>
        $('.bootstrap-select').selectpicker();

        window.data_url = '{{url('panel/courses/datatable')}}';

        window.columns = [
            // {
            //     field: '',
            //     title: '',
            //     width: 40,
            //     sortable: false,
            //     // type: 'number',
            //     selector: {class: 'kt-checkbox--solid', name: 'admins'},
            // },

            {
                field: 'image',
                title: "صورة الدورة",
                'class': "text-center",
                textAlign: "center",
                sortable : false,
                template: function (data) {
                    if (data.image)
                        return '<img src="{{ url('image/')  }}/' + data.image + '/60x60"/>';
                    else
                        return '<img src="{{ url('image/admins/default.jpg/60x60')  }}"/>'
                }
            },
            {
                field: 'title',
                title: "العنوان",
                textAlign: "center",

            },
            {
                field: 'catagory_id',
                title: "التصنيف",
                textAlign: "center",
                template: function (data) {
                    return data.catagory ? data.catagory.name : '-';
                }
            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                width: 100,
                template: function (data) {
                    var lecturesUrl = "{{ url('panel/courses/') }}/" + data.id + "/lectures";
                    var editUrl = "{{ url('panel/courses/') }}/" + data.id + "/edit";
                    var deleteUrl = "{{ url('panel/courses/') }}/" + data.id;
                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="` + lecturesUrl + `"><i class="la la-book"></i> المناهج</a>
                                    <a class="dropdown-item" href="` + editUrl + `"><i class="la la-edit"></i> تعديل</a>
                                    <a class="dropdown-item delete" href="#" data-url="` + deleteUrl + `"><i class="la la-times"></i> حذف </a>
						  	</div>
						</div>`;
                },
            }
        ];

        $('#search').on('click', function () {

            var query = datatable.getDataSourceQuery();
            query.title = $('#title').val().toLowerCase();
            query.catagory_id = $('#catagory_id').val().toLowerCase();

            datatable.setDataSourceQuery(query);
            datatable.load();
        });
        $('#cancelSearch').on('click', function () {
            $('form#searchForm').resetForm();
            $(".bootstrap-select").selectpicker("refresh");

            var query = datatable.getDataSourceQuery();

            query.title = $('#title').val().toLowerCase();
            query.catagory_id = $('#catagory_id').val().toLowerCase();

            datatable.setDataSourceQuery(query);
            datatable.load();
        });

    </script>

    <script>

    </script>
@endpush
