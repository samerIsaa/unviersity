@extends('panel.layout.master' , ['title' => 'إضافة مدرس'])


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">إضافة مدرس</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <form class="kt-form" id="kt_form_1">
        @csrf
        <div class="row">

            <div class="col-md-9">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                إضافة مدرس
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>الإسم</label>
                            <input class="form-control m-input" type="text" name="name" placeholder="الإسم">
                        </div>
                        <div class="form-group">
                            <label>البريد الإلكتروني</label>
                            <input class="form-control m-input" type="email" name="email"
                                   placeholder="البريد الإلكتروني">
                        </div>
                        <div class="form-group">
                            <label>رقم الجوال</label>
                            <input class="form-control m-input" type="text" name="mobile"
                                   placeholder="رقم الجوال">
                        </div>
                        <div class="form-group">
                            <label>كلمة المرور</label>
                            <input class="form-control m-input" type="password" name="password" id="password"
                                   placeholder="كلمة المرور">
                        </div>
                        <div class="form-group">
                            <label>تاكيد كلمة المرور</label>
                            <input class="form-control m-input" type="password" name="password_confirmation"
                                   placeholder="تاكيد كلمة المرور">
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-lg-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label" style="width: 100%;">
                            <button type="submit" style="width: 100%;" id="save" class="btn btn-brand">إضافة</button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>صورة المدرس</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgload" name="avatar">
                                <label class="custom-file-label" for="imgload" id="imgload">Choose file</label>
                            </div>
                        </div>

                        <div class="img-responsive">
                            <div class="imageEditProfile">
                                <img src="" alt="" id="imgshow" style="max-width: 100%">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </form>

@endsection


@push('js')

    <script>
        $('.bootstrap-select').selectpicker();

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $("#kt_form_1").validate({
                    // define validation rules
                    rules: {
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 6
                        },
                        password_confirmation: {
                            required: true,
                            equalTo: "#password",
                        },

                    },

                    //display error alert on form submit
                    invalidHandler: function (event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            };


            return {
                // public functions
                init: function () {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });


        $('#kt_form_1').submit(function (e) {
            e.preventDefault();

            if ($(this).valid()) {
                $('#save').attr('disabled', 'disabled')
                    .html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

                var fd = new FormData(this);

                $.ajax({
                    url: "{{ route('panel.teachers.store') }}",
                    method: 'POST',
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (response) {
                        $('#save').removeAttr('disabled').html('إضافة');
                        swal.fire({
                            type: 'success',
                            title: response.msg,
                            confirmButtonText: 'موافق'
                        }).then((result) => {
                            window.location = "{{ route('panel.teachers.index') }}";
                        });
                    },
                    error: function (response) {
                        $('#save').removeAttr('disabled').html('إضافة');
                        swal.fire(response.responseJSON.msg, "", "error");
                    }
                });
            }
        })


    </script>
    <script>
        $("#imgload").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgshow').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });

    </script>
@endpush
