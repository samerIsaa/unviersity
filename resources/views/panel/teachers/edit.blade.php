@extends('panel.layout.master' , ['title' => 'تعديل مدرس'])


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">تعديل مدرس</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <form class="kt-form" action="" method="post" id="kt_form_1">
        @csrf
        @method('PUT')

        <div class="row">

            <div class="col-md-9">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                تعديل مدرس
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>الإسم</label>
                            <input class="form-control m-input" type="text" name="name" placeholder="الإسم" value="{{ $teacher->name }}">

                        </div>
                        <div class="form-group">
                            <label>البريد الإلكتروني</label>
                            <input class="form-control m-input" type="email" name="email"
                                   placeholder="البريد الإلكتروني"  value="{{ $teacher->email }}">
                        </div>
                        <div class="form-group">
                            <label>رقم الجوال</label>
                            <input class="form-control m-input" type="text" name="mobile"
                                   placeholder="رقم الجوال" value="{{ $teacher->mobile }}">
                        </div>
                        <div class="form-group">
                            <label>كلمة السر</label>
                            <input class="form-control m-input" type="password" name="password" id="password"
                                   placeholder="كلمة السر">
                        </div>
                        <div class="form-group">
                            <label>تاكيد كلمة المرور</label>
                            <input class="form-control m-input" type="password" name="password_confirmation"
                                   placeholder="تاكيد كلمة المرور">
                        </div>

                    </div>


                    <!--end::Form-->
                </div>

                <!--end::Portlet-->

            </div>

            <div class="col-lg-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label"  style="width: 100%;">
                            <button type="submit"  style="width: 100%;" id="save" class="btn btn-brand">تعديل</button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>صورة المدرس</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgload" name="avatar">
                                <label class="custom-file-label" for="imgload" id="imgload">Choose file</label>
                            </div>
                        </div>

                        <div class="img-responsive">
                            <div class="imageEditProfile">
                                <img src="@if ($teacher->avatar) {{ url('image/'. $teacher->avatar) }} @endif" alt="" id="imgshow" style="max-width: 100%">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </form>

@endsection


@push('js')

    <script>

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $("#kt_form_1").validate({
                    // define validation rules
                    rules: {
                        name: {
                            required: true,
                        },
                        email:{
                          required : true,
                          email: true
                        },
                        password: {
                            minlength: 6
                        },
                        password_confirmation: {
                            minlength: 6,
                            equalTo: "#password",
                        },

                    },

                    //display error alert on form submit
                    invalidHandler: function (event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            }


            return {
                // public functions
                init: function () {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });

        $('#kt_form_1').submit(function (e) {
            e.preventDefault();

            $('#save').attr('disabled', 'disabled')
                .html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

            var fd = new FormData(this);


            $.ajax({
                url : "{{ route('panel.teachers.update' , $teacher->id ) }}",
                method: 'POST',
                contentType: false,
                processData: false,
                data : fd,
                success : function (response) {
                    $('#save').removeAttr('disabled').html('تعديل');
                    swal.fire({
                        type: 'success',
                        title: response.msg,
                        confirmButtonText: 'موافق'
                    }).then((result) => {
                        window.location = "{{ route('panel.teachers.index') }}";
                    });
                },
                error: function (response) {
                    $('#save').removeAttr('disabled').html('تعديل');
                    swal.fire(response.responseJSON.msg, "", "error");

                }
            });


        });
    </script>


@endpush
