@extends('panel.layout.master' , ['title' => 'الطلاب'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('panelAssets/css/fancybox.min.css') }}">

@endpush


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الطلاب</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-users"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    الطلاب
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">

                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="بحث" id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="kt-datatable text-center" id="ajax_data"></div>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('panelAssets/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>
    <script src="{{ asset('panelAssets/js/fancybox.min.js') }}"></script>


    <script>

        window.data_url = '{{url('panel/students/datatable')}}';

        window.columns = [
            // {
            //     field: '',
            //     title: '',
            //     width: 40,
            //     sortable: false,
            //     // type: 'number',
            //     selector: {class: 'kt-checkbox--solid', name: 'admins'},
            // },

            {
                field: 'avatar',
                title: "صورة الطالب",
                textAlign: "center",
                template: function (data) {
                    if (data.avatar) {
                        return '<a href="{{ url('image/') }}/' + data.avatar + '" data-fancybox ><img  src="{{ url('image/') }}/' + data.avatar + '/80x80" style="border-radius:50%" ></a>';
                    } else {
                        return '<a href="{{ url('image/teachers/default.jpg') }}" data-fancybox ><img  src="{{ url('image/teachers/default.jpg/80x80') }}" style="border-radius:50%" ></a>';
                    }

                },
            },
            {
                field: 'name',
                title: "الإسم",
                textAlign: "center",
            },
            {
                field: 'u_id',
                title: "الرقم الجامعي",
                textAlign: "center",
            },
            {
                field: 'email',
                title: "البريد الإلكتروني",
                textAlign: "center",
            },
            {
                field: 'is_blocked',
                title: "محظور ؟",
                textAlign: "center",
                sortable: false,
                template: function (data) {
                    var url = "{{ url('panel/students/') }}/" + data.id + "/changeBlockStatus";
                    var checked = data.is_blocked == 1 ? "checked" : "";
                    return `<span class="kt-switch kt-switch--icon">
                            <label>
                                <input type="checkbox" id="isBlocked"
                                       data-url="` + url + `" ` + checked + `>
                                <span></span>
                            </label>
                        </span>`;
                }
            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                width: 100,
                template: function (data) {
                    var deleteUrl = "{{ url('panel/students/') }}/" + data.id;
                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item delete" href="#" data-url="` + deleteUrl + `"><i class="la la-times"></i> حذف </a>
						  	</div>
						</div>`;
                },
            }
        ];


        $(document).on('change', '#isBlocked', function (event) {
            var update_url = $(this).data('url');
            event.preventDefault();
            $.ajax({
                url: update_url,
                method: 'POST',
                type: 'json',
                success: function (response) {
                    if (response.status == 200) {
                        swal.fire({
                            title: response.msg,
                            type: 'success',
                            cancelButtonText: false,
                            confirmButtonText: 'موافق',
                            dismiss: false
                        })
                    } else {
                        swal.fire("Error !", response.msg, "error");
                    }
                },
                error: function (response) {
                        swal.fire(response.responseJSON.msg, '', "error");
                }
            });
        });


    </script>

@endpush
